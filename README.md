## Installation

```bash
npm i
ng s
```

## Features

- List all todos.
- Delete / Mark as done action for a single item and selected items as well.
- Open details for every single item.
- Add new Todo.
- Filter todos by date, title, and group name.

## Missing Features

- Side menu actions not testable and maybe not work correctly.

## Time

- 20 min: Project setup
- 3 h: refresh angular.
- 8 h implement the idea.
- 4 h refactor the code to be more readable and enhance the performance.
