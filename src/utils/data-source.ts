export const SIDE_MENU_ITEMS = [
  {
    icon: "home.svg",
    label: "Home",
    isDropdown: false,
  },
  {
    icon: "list.svg",
    label: "All",
    isDropdown: false,
  },
  {
    icon: "calendar.svg",
    label: "Today",
    isDropdown: false,
  },
  {
    icon: "calendar-1.svg",
    label: "Week",
    isDropdown: false,
  },
  {
    icon: "check.svg",
    label: "Done",
    isDropdown: false,
  },
  {
    icon: "delete.svg",
    label: "Deleted",
    isDropdown: false,
  },
  {
    icon: "folder-white-shape.svg",
    label: "Groups",
    isDropdown: true,
  },
];
export const NEW_TASK_BUTTON = {
  label: "New Task",
  icon: "plus-button.svg",
  bgColor: "#183c5b",
};

export const DONE_BUTTON = {
  label: "Done",
  icon: "tick-1.svg",
  bgColor: "#86e0a4",
};

export const DELETE_SELCTED_BUTTON = {
  label: "Delete Selected",
  icon: "delete_white.svg",
  bgColor: "#ff9c9c",
};

export const ADD_NEW_TASK_BUTTON = {
  label: "Add New",
  bgColor: "#86e0a4",
};

export const RESET_FORM_BUTTON = {
  label: "Reset",
  bgColor: "#ff9c9c",
};

export const BACK_TO_BUTTON = {
  label: "Back To List",
  bgColor: "#183c5b",
};

export const TODOS_LIST = [
  {
    id: "1",
    group: "Group 1",
    items: [
      {
        id: "1_1",
        name: "Coffee bears",
        status: "Done",
        date: "1/7/2021",
      },
      {
        id: "1_2",
        name: "Milk",
        date: "1/11/2021",
      },
    ],
  },
  {
    id: "2",
    group: "Group 2",
    items: [
      {
        id: "2_1",
        name: "Mortgage Repayment",
        date: "1/14/2021",
      },
    ],
  },
  {
    id: "3",
    group: "OUR HOME TO-DOS",
    items: [
      {
        id: "3_1",
        name: "Mortgage Repayment",
        status: "Done",
        date: "1/12/2021",
      },
      {
        id: "3_2",
        name: "Mortgage Repayment",
        date: "1/28/2021",
      },
      {
        id: "3_3",
        name: "Mortgage Repayment",
        date: "1/28/2021",
      },
    ],
  },
];

export const TODOSLIST = [
  {
    id: "1",
    title: "Coffee bears",
    description: "",
    group: "1",
    groupId: "1",
    piriorty: "high",
    deliveryDate: new Date(),
    complete: false,
    checked: false,
  },

  {
    id: "2",
    title: "Coffee bears  2",
    description: "",
    group: "2",
    piriorty: "high",
    deliveryDate: new Date(),
    complete: true,
    checked: false,
  },
  {
    id: "3",
    title: "Coffee bears  3",
    description: "",
    group: "2",
    piriorty: "high",
    deliveryDate: new Date(),
    complete: false,
    checked: false,
  },
  {
    id: "4",
    title: "Coffee bears  3",
    description: "",
    group: "3",
    piriorty: "high",
    deliveryDate: new Date(),
    complete: false,
    checked: false,
  },
];
export const NEW_TASK_FORM_SCHEMA = [
  {
    form_control_type: "text",
    placeholder: "Title",
    name: "title",
    isRequired: true,
  },
  {
    form_control_type: "text-area",
    placeholder: "Description",
    name: "description",
    isRequired: true,
  },
  {
    form_control_type: "radio-buttons",
    placeholder: "Priority",
    name: "priority",
    isRequired: true,
    options: [
      {
        label: "High",
        id: "high",
      },
      {
        label: "Meduim",
        id: "meduim",
      },
      {
        label: "Low",
        id: "low",
      },
    ],
  },
  {
    form_control_type: "date-picker",
    placeholder: "Delivery date",
    name: "delivery-date",
    isRequired: true,
  },
  {
    form_control_type: "single-select",
    placeholder: "Group",
    name: "group",
    isRequired: true,
    options: [
      { label: "Group 1", id: "group_1" },
      { label: "Group 2", id: "group_2" },
      { label: "Group 3", id: "group_3" },
      { label: "Group 4", id: "group_4" },
    ],
  },
];

export const GROUPS = [
  { label: "Group 1", id: "1" },
  { label: "Group 2", id: "2" },
  { label: "OUR HOME TO-DOS", id: "3" },
];
