import { Component, OnInit } from "@angular/core";
import {
  NEW_TASK_BUTTON,
  DONE_BUTTON,
  DELETE_SELCTED_BUTTON,
} from "./../../../utils/data-source";
import { DataCrudService } from "./../../../app/services/data-crud.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  newTaskButton = NEW_TASK_BUTTON;
  doneButton = DONE_BUTTON;
  deleteSelectionButton = DELETE_SELCTED_BUTTON;

  constructor(private dataCrudService: DataCrudService) {}
  ngOnInit() {}

  get test() {
    return this.dataCrudService.getAllTodos();
  }
  get todos() {
    return this.dataCrudService.getAllTodos();
  }

  get todosGroups() {
    return this.dataCrudService.getTodosGroupName();
  }

  get selectedTodos() {
    return this.dataCrudService.getSelectedTodos();
  }

  markAsDone() {
    this.selectedTodos.map((selectedTodo) =>
      this.dataCrudService.updateTodoById(selectedTodo.id, {
        complete: true,
        checked: false,
      })
    );
  }

  deleteItems() {
    this.selectedTodos.map((selectedTodo) =>
      this.dataCrudService.deleteTodoById(selectedTodo.id)
    );
  }
}
