import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { DataCrudService } from "./../../../app/services/data-crud.service";
import { Todo } from "./../../classes/todo";

@Component({
  selector: "app-todo-details",
  templateUrl: "./todo-details.component.html",
  styleUrls: ["./todo-details.component.scss"],
})
export class TodoDetailsComponent implements OnInit, OnDestroy {
  id: number;
  todo: Todo;
  formatedDate: any;
  private subscription: Subscription;
  constructor(
    private route: ActivatedRoute,
    private dataCrudService: DataCrudService
  ) {}

  ngOnInit() {
    this.subscription = this.route.params.subscribe((params) => {
      this.todo = this.dataCrudService.getTodoById(params["id"]);
      this.formatedDate = this.todo.deliveryDate.toLocaleDateString("en-US");
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
