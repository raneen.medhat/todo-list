import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Router } from "@angular/router";
import { v4 as uuidv4 } from "uuid";
import {
  GROUPS,
  ADD_NEW_TASK_BUTTON,
  RESET_FORM_BUTTON,
  BACK_TO_BUTTON,
} from "./../../../utils/data-source";
import { DataCrudService } from "./../../services/data-crud.service";

@Component({
  selector: "app-new-task",
  templateUrl: "./new-task.component.html",
  styleUrls: ["./new-task.component.scss"],
})
export class NewTaskComponent implements OnInit {
  addNewTaskForm: FormGroup;
  datepickerModel: Date;
  groups = GROUPS;
  addNewTaskBtn = ADD_NEW_TASK_BUTTON;
  resetFormBtn = RESET_FORM_BUTTON;
  backToListBtn = BACK_TO_BUTTON;
  submitted = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private dataCrudService: DataCrudService
  ) {}

  onChangeGroup(group) {
    console.log("group ", group);
  }

  onClear() {
    console.log("cleared");
  }

  formInit() {
    this.addNewTaskForm = this.fb.group({
      title: ["", Validators.required],
      description: ["", Validators.required],
      group: ["", Validators.required],
      piriorty: ["", Validators.required],
      deliveryDate: ["", Validators.required],
    });
  }
  onSubmit(form: FormGroup) {
    if (form.valid) {
      this.dataCrudService.addTodo({
        id: uuidv4(),
        ...form.value,
        checked: false,
        complete: false,
      });
      this.addNewTaskForm.reset();
    }
  }

  navigateTo(path) {
    this.router.navigateByUrl(path);
  }
  ngOnInit(): void {
    this.formInit();
  }
}
