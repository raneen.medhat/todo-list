import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { DataCrudService } from "./../../services/data-crud.service";

@Component({
  selector: "app-item-card",
  templateUrl: "./item-card.component.html",
  styleUrls: ["./item-card.component.scss"],
})
export class ItemCardComponent {
  @Input() item;
  @Input() todos;
  @Output() onItemSelcted = new EventEmitter<any>();
  constructor(
    private router: Router,
    private dataCrudService: DataCrudService
  ) {}

  onChange(item) {
    this.dataCrudService.updateTodoById(item.id, {
      checked: !item.checked,
    });
  }

  deleteItem(event, id) {
    event.stopPropagation();
    this.dataCrudService.deleteTodoById(id);
  }

  markAsDone(event, item) {
    event.stopPropagation();
    this.dataCrudService.toggleTodoComplete(item);
  }

  goToDetails(id) {
    this.router.navigateByUrl(`/todo-details/${id}`);
  }
}
