import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { SIDE_MENU_ITEMS } from "./../../../utils/data-source";
import { DataCrudService } from "./../../services/data-crud.service";

@Component({
  selector: "app-side-nav",
  templateUrl: "./side-nav.component.html",
  styleUrls: ["./side-nav.component.scss"],
})
export class SideNavComponent {
  sideMenuItems: Array<Object>;
  imgPath: any;

  constructor(
    private router: Router,
    private dataCrudService: DataCrudService
  ) {
    this.sideMenuItems = SIDE_MENU_ITEMS;
    this.imgPath = "./../../../assets/icons/";
  }

  handleClick(label) {
    switch (label) {
      case "Home":
        this.router.navigateByUrl("/");
        break;

      case "All":
        this.router.navigateByUrl("/");
        this.dataCrudService.getAllTodos();
        break;

      case "Today":
        this.router.navigateByUrl("/");
        this.dataCrudService.filterByDate(new Date());
        break;

      case "Done":
        this.router.navigateByUrl("/");
        this.dataCrudService.getDoneTodos();

      default:
        return;
    }
  }
}
