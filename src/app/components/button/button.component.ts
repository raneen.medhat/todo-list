import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"],
})
export class ButtonComponent implements OnInit {
  @Input() type;
  @Input() buttonData;
  @Input() isDisabled;
  @Input() redirectTo;
  @Output() onBtnClick = new EventEmitter<any>();
  status: Boolean;
  imgPath: any;
  selectedItems = [];
  constructor(private router: Router) {
    this.imgPath = "./../../../assets/icons/";
    this.status = true;
  }

  onbtnClick() {
    this.onBtnClick.emit(this.selectedItems);
    this.redirectTo && this.router.navigateByUrl(this.redirectTo);
  }
  ngOnInit(): void {}
}
