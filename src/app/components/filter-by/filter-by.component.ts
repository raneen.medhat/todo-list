import { Component } from "@angular/core";
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { DataCrudService } from "./../../../app/services/data-crud.service";
@Component({
  selector: "app-filter-by",
  templateUrl: "./filter-by.component.html",
  styleUrls: ["./filter-by.component.scss"],
})
export class FilterByComponent {
  bsConfig: Partial<BsDatepickerConfig>;
  selectedGroup: number;
  groups = this.dataCrudService.getTodosGroupName();
  datepickerModel: Date;

  constructor(private dataCrudService: DataCrudService) {
    this.bsConfig = Object.assign({}, { containerClass: "theme-dark-blue" });
  }
  onChange(item) {
    this.dataCrudService.filterByGroup(item);
  }
  onChangeTitel(charcters) {
    this.dataCrudService.filterByTitle(charcters);
  }
  onChangeDate(date) {
    this.dataCrudService.filterByDate(date);
  }
  onClear() {
    this.dataCrudService.getAllTodos();
  }
}
