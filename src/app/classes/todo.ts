export class Todo {
  id: string;
  title: string = "";
  description: string = "";
  group: string = "";
  piriorty: string = "";
  deliveryDate: Date = new Date();
  complete: boolean = false;
  checked: boolean = false;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
