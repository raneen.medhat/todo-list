import { Component, OnInit } from "@angular/core";
import { DataCrudService } from "./../app/services/data-crud.service";
import { Todo } from "./classes/todo";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  ngOnInit() {}
  constructor(private dataCrudService: DataCrudService) {}

  newTodo: Todo = new Todo();

  addTodo() {
    this.dataCrudService.addTodo(this.newTodo);
    this.newTodo = new Todo();
  }

  toggleTodoComplete(todo) {
    this.dataCrudService.toggleTodoComplete(todo);
  }

  removeTodo(todo) {
    this.dataCrudService.deleteTodoById(todo.id);
  }

  get todos() {
    return [];
  }
}
