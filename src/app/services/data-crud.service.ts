import { Injectable } from "@angular/core";
import { Todo } from "./../classes/todo";
import { TODOSLIST, GROUPS } from "./../../utils/data-source";
@Injectable({
  providedIn: "root",
})
export class DataCrudService {
  todos: Todo[] = TODOSLIST;
  groups = GROUPS;
  lastId: number = 0;
  todosCopy = JSON.parse(JSON.stringify(this.todos));

  constructor() {}

  addTodo(todo: Todo): DataCrudService {
    this.todos.push(todo);
    return this;
  }

  deleteTodoById(id: string): DataCrudService {
    this.todos = this.todos.filter((todo) => todo.id !== id);
    return this;
  }

  updateTodoById(id: string, values: Object = {}): Todo {
    let todo = this.getTodoById(id);
    if (!todo) {
      return null;
    }
    Object.assign(todo, values);
    return todo;
  }

  getAllTodos() {
    return this.todos.reduce((result, currentValue) => {
      (result[currentValue["group"]] =
        result[currentValue["group"]] || []).push(currentValue);
      return result;
    }, {});
  }

  getTodosGroupName() {
    return this.groups;
  }

  getTodoById(id: string): Todo {
    return this.todos.filter((todo) => todo.id === id).pop();
  }

  toggleTodoComplete(todo: Todo) {
    let updatedTodo = this.updateTodoById(todo.id, {
      complete: !todo.complete,
    });
    return updatedTodo;
  }

  getSelectedTodos() {
    return this.todos.filter((todo) => todo.checked);
  }

  getDoneTodos() {
    return this.todos.filter((todo) => todo.complete);
  }

  filterByGroup(group) {
    return (this.groups = [group]);
  }

  filterByTitle(input) {
    if (input.length != 0) {
      this.todos = this.todos.filter((item) =>
        item.title.toLowerCase().includes(input.toLowerCase())
      );
      return this;
    } else {
      this.todos = this.todosCopy;
    }
  }

  filterByDate(date) {
    if (date) {
      this.todos = this.todos.filter(
        (item) =>
          new Date(item.deliveryDate).getFullYear() ===
            new Date(date).getFullYear() &&
          new Date(item.deliveryDate).getMonth() ===
            new Date(date).getMonth() &&
          new Date(item.deliveryDate).getDate() === new Date(date).getDate()
      );
      return this;
    } else {
      this.todos = this.todosCopy;
    }
  }
}
