import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { SideNavComponent } from "./components/side-nav/side-nav.component";
import { MenuListComponent } from "./components/menu-list/menu-list.component";
import { CollapseModule } from "ngx-bootstrap/collapse";
import { AppHeaderComponent } from "./components/app-header/app-header.component";
import { ItemCardComponent } from "./components/item-card/item-card.component";
import { ButtonComponent } from "./components/button/button.component";
import { FilterByComponent } from "./components/filter-by/filter-by.component";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { NgSelectModule } from "@ng-select/ng-select";
import { FormsModule } from "@angular/forms";
import { NgxCheckboxModule } from "ngx-checkbox";
import { NewTaskComponent } from "./pages/new-task/new-task.component";
import { HomeComponent } from "./pages/home/home.component";
import { TodoDetailsComponent } from "./pages/todo-details/todo-details.component";

@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    MenuListComponent,
    AppHeaderComponent,
    ItemCardComponent,
    ButtonComponent,
    FilterByComponent,
    NewTaskComponent,
    HomeComponent,
    TodoDetailsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TooltipModule.forRoot(),
    CollapseModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgSelectModule,
    FormsModule,
    NgxCheckboxModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
