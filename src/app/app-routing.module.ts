import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { NewTaskComponent } from "./pages/new-task/new-task.component";
import { TodoDetailsComponent } from "./pages/todo-details/todo-details.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "new-task", component: NewTaskComponent },
  { path: "todo-details/:id", component: TodoDetailsComponent },

  // { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
